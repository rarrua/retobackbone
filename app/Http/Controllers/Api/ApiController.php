<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ZipCodeResource;
use App\Imports\CodigosPostalesImport;
use App\Models\CodigosPostales;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class ApiController extends Controller
{
    public function importExcel()
    {
        Excel::import(new CodigosPostalesImport(), public_path('CPdescarga.xls'));

        return response()->json(['msg'=>'Insertado correctamente']);
    }

    public function getZipCode($zip_code)
    {
        if ($zip_code)
        {

            $codigos = CodigosPostales::where('d_codigo',$zip_code)->get();

            $first = $codigos->first();

            $result = array(
                'zip_code'=> $first->d_codigo,
                'locality'=> strtoupper($first->d_ciudad),
                'federal_entity'=>
                    [
                        'key'=> $first->c_estado,
                        'name'=> strtoupper($first->d_estado),
                        'code'=> $first->c_cp
                    ],
                'settlements' => array(),
                'municipality' =>
                    [
                        'key'=>$first->c_mnpio,
                        'name'=>strtoupper($first->d_mnpio)
                    ]
            );

            foreach ($codigos as $codigo)
            {
                $result['settlements'][] = [
                    'key' => $codigo->id_asenta_cpcons,
                    'name'=> strtoupper($codigo->d_asenta),
                    'zone_type' => strtoupper($codigo->d_zona),
                    'settlement_type' => [
                        'name'=>$codigo->d_tipo_asenta
                    ]
                ];
            }


            return response()->json($result, '200',['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                JSON_UNESCAPED_UNICODE);

        }

        return response()->json(['error'=>'El codigo zip no puede estar vacio','status'=>409]);
    }
}
