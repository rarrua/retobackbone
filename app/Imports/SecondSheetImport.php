<?php

namespace App\Imports;

use App\Models\CodigosPostales;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class SecondSheetImport implements ToCollection, WithHeadingRow
{

    public function collection(Collection $collection)
    {
        foreach ($collection as $row)
        {
            CodigosPostales::create([
                'd_codigo' => $row['d_codigo'],
                'd_asenta' => $row['d_asenta'],
                'd_tipo_asenta' => $row['d_tipo_asenta'],
                'd_mnpio' => $row['d_mnpio'],
                'd_estado' => $row['d_estado'],
                'd_ciudad' => $row['d_ciudad'],
                'd_cp' => $row['d_cp'],
                'c_estado' => $row['c_estado'],
                'c_oficina' => $row['c_oficina'],
                'c_cp' => $row['c_cp'],
                'c_tipo_asenta' => $row['c_tipo_asenta'],
                'c_mnpio' => $row['c_mnpio'],
                'id_asenta_cpcons' => $row['id_asenta_cpcons'],
                'd_zona' => $row['d_zona'],
                'c_cve_ciudad' => $row['c_cve_ciudad'],
            ]);
        }
    }
}
