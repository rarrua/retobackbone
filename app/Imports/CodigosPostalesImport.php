<?php

namespace App\Imports;

use App\Models\CodigosPostales;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class CodigosPostalesImport implements ToCollection, WithHeadingRow
{

    public function collection(Collection $collection)
    {
        $unwanted_array = array(    'Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
            'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U',
            'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c',
            'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o',
            'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y' );


        foreach ($collection as $row)
        {
            CodigosPostales::create([
                'd_codigo' => $row['d_codigo'],
                'd_asenta' => strtr( $row['d_asenta'], $unwanted_array ),
                'd_tipo_asenta' => strtr($row['d_tipo_asenta'], $unwanted_array),
                'd_mnpio' => strtr( $row['d_mnpio'], $unwanted_array),
                'd_estado' => strtr($row['d_estado'] , $unwanted_array),
                'd_ciudad' => strtr($row['d_ciudad'], $unwanted_array),
                'd_cp' => $row['d_cp'],
                'c_estado' => $row['c_estado'],
                'c_oficina' => $row['c_oficina'],
                'c_cp' => $row['c_cp'],
                'c_tipo_asenta' => $row['c_tipo_asenta'],
                'c_mnpio' => $row['c_mnpio'],
                'id_asenta_cpcons' => $row['id_asenta_cpcons'],
                'd_zona' => $row['d_zona'],
                'c_cve_ciudad' => $row['c_cve_ciudad'],
            ]);
        }
    }
}
