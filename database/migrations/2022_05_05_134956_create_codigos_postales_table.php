<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('codigos_postales', function (Blueprint $table) {
            $table->id();
            $table->string('d_codigo','35')->index()->nullable();
            $table->string('d_asenta','100')->nullable();
            $table->string('d_tipo_asenta','35')->nullable();
            $table->string('d_mnpio','100')->nullable();
            $table->string('d_estado','100')->nullable();
            $table->string('d_ciudad','100')->nullable();
            $table->mediumInteger('d_cp')->nullable();
            $table->tinyInteger('c_estado')->nullable();
            $table->mediumInteger('c_oficina')->nullable();
            $table->mediumInteger('c_cp')->nullable();
            $table->tinyInteger('c_tipo_asenta')->nullable();
            $table->mediumInteger('c_mnpio')->nullable();
            $table->mediumInteger('id_asenta_cpcons')->nullable();
            $table->string('d_zona','35')->nullable();
            $table->mediumInteger('c_cve_ciudad')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('codigos_postales');
    }
};
